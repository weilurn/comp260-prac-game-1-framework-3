﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	private BeeSpawner beeSpawner;

	// Use this for initialization
	void Start () {
		//find the bee spawner and store a reference for later
		beeSpawner = FindObjectOfType<BeeSpawner>();
	}
	// outer class definition omitted

	public Vector2 move;

	public Vector2 velocity; // in metres per second

	public float maxSpeed = 5.0f; //in m/s
	public float acceleration = 10.0f;// in m/s/s
	public float brake = 5.0f; // in m/s/s
	public float turnSpeed = 30.0f; //degrees/s

	private float speed = 0.0f; // in m/s

	public float destroyRadius = 1.0f;

	//public int playerNumber;



	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Fire1")) {
			//destroy nearby bees
			beeSpawner.DestroyBees (
				transform.position, destroyRadius);
		}
		//the horizontal axis controls the turn
		float turn = Input.GetAxis ("Horizontal");

		//turn car
		//transform.Rotate (0, 0, -turn * turnSpeed * Time.deltaTime);

		//the vertical axis controls acceleration fwd/back
		float forwards = Input.GetAxis ("Vertical");
		

		if (forwards > 0) {
			//accelerate forwards
			transform.Rotate(0,0, -turn * turnSpeed * speed * Time.deltaTime);

			speed = speed + acceleration * Time.deltaTime;
		} 
		else if (forwards < 0) {
			//accelerate backwards
			transform.Rotate(0,0, -turn * turnSpeed * speed * Time.deltaTime);

			speed = speed - acceleration * Time.deltaTime;
		} else {
			// braking
			if (speed > 0) {
				speed = Mathf.Max(speed - brake * Time.deltaTime, 0);
			} else {
			if(speed < 0)
				speed = Mathf.Min(speed + brake * Time.deltaTime, 0);
			}
		}

		//clamp the speed
		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);


		//compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;

		//move the object
		if (forwards != 0) {
			transform.Translate (velocity * Time.deltaTime, Space.Self);
		}
	}
}
		
		// get the input values
	//	Vector2 direction;
	//	if (playerNumber == 1) {
	//		direction.x = Input.GetAxis ("Horizontal");
	//		direction.y = Input.GetAxis ("Vertical");
	//	}else{
	//		direction.x = Input.GetAxis ("Horizontal1");
	//		direction.y = Input.GetAxis ("Vertical1");
	//	}
		// scale by the maxSpeed parameter
	//	Vector2 velocity = direction * maxSpeed;

		// move the object
	//	transform.Translate (velocity * Time.deltaTime);


